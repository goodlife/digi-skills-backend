<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('website');
            $table->string('address');
            $table->string('city');
            $table->string('postal_code');
            $table->string('year_establish');
            $table->string('category');
            $table->string('country_of_operation')->nullable();;
            $table->string('logo')->nullable();;
            $table->text('about')->nullable();;
            $table->text('program_duration')->nullable();;
            $table->text('description')->nullable();;
            $table->text('additional_remarks')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
