<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->id();
            $table->string('program_name');
            $table->integer('organization_id');
            $table->string('type');
            $table->string('availabilty');
            $table->string('location');
            $table->string('about');
            $table->string('image');
            $table->string('graduants')->nullable();
            $table->string('awards')->nullable();
            $table->string('banner_image')->nullable();;
            $table->string('age_requirements')->nullable();;
            $table->string('partcipation_guidelines')->nullable();;
            $table->string('qualification')->nullable();;
            $table->string('exprience')->nullable();;
            $table->string('created_by')->nullable();;
            $table->string('application_details');
            $table->string('additional_requirements')->nullable();
            $table->boolean('featured')->default(0);
            $table->boolean('activated')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
