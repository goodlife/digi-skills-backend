<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Model\Admin;
use App\Models\User;
use App\Models\RoleUsers;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
     $user = User::create([
            'name' => 'Kosgei Wilson Kipkoech',
            'email' => 'kosgeiwil95@gmail.com',
            'password' => bcrypt('123456789'),
            'phone'=>'0717890426',
        ]);

        $role = RoleUsers::create([
            'user_id' => $user->id,
            'role_id' => 1,
        ]);
    }
}
