@extends('admin_panel.layouts.app')

@section('Title','Index Page')
  @section('content')
  <!--.row-->

  <div class="row">
      <br><br><br>
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading"> Update Programs</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    @if ($message = Session::get('message'))
                    <div class="alert alert-success alert-block mt-2"> <button type="button"
                            class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong></div>
                @endif

                    <form action="{{route('update-program',$program->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <h3 class="box-title">Program  Information</h3>
                            <hr>
                            <div class="row">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Program Name</label>
                                            <input type="text" id="firstName" name="name"  value="{{$program->program_name}}"  class="form-control" placeholder="Enter Program name"> </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Organization</label>
                                            <select class="form-control" name="organization_id">
                                                @foreach ($organization as $organizations)
                                                        <option value="{{ $organizations->id }}">
                                                            {{ $organizations->name }}
                                                        </option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Image</label>
                                            <input type="file"  class="form-control" name="image" value="{{$program->image}}" > </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Banner Image</label>
                                            <input type="file"  class="form-control" name="banner_image" value="{{$program->banner_image}}"> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Type</label>
                                            <input type="text" class="form-control" name="type" value="{{$program->type}}"> </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Program Availability</label>
                                            <input type="text" class="form-control" name="availabilty" value="{{$program->availabilty}}"> </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->

                                <!--/row-->
                                <!--/span-->
                            </div>
                            <!--/row-->
                              <!--/row-->
                              <h3 class="box-title m-t-40">Program status</h3>
                              <hr>
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label">Featured</label>
                                          <select class="form-control" name="featured">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                        </div>
                                  </div>
                                  <!--/span-->
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label">Activated</label>
                                          <select class="form-control" name="activated">
                                            <option activate value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                        </div>
                                  </div>
                                  <!--/span-->
                              </div>
                            <!--/row-->
                            <h3 class="box-title m-t-40">Program details</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" class="form-control" name="location" value="{{$program->location}}"> </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Age requirements</label>
                                        <input type="text" class="form-control" name="age_requirements" value="{{$program->age_requirments}}"> </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Participation Guidelines</label>
                                        <input type="textarea" class="form-control" name="partcipation_guidelines" value="{{$program->partcipation_guidelines}}"> </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">About Program</label>
                                        <input type="textarea" class="form-control" name="about" value="{{$program->about}}"> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Qualification</label>
                                        <input type="textarea" class="form-control" name="qualification" value="{{$program->qualification}}"> </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Experience</label>
                                        <input type="textarea" class="form-control" name="exprience" value="{{$program->experience}}"> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Application Details</label>
                                        <input type="textarea" class="form-control" name="application_details" value="{{$program->application_details}}"> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Additianal Requirements</label>
                                        <input type="textarea" class="form-control" name="additional_requirements" value="{{$program->additional_requirements}}"> </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            {{-- <button type="button" class="btn btn-default">Cancel</button> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--./row-->

  @endsection
