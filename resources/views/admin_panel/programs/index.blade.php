@extends('admin_panel.layouts.app')
@section('Title', 'Index Page')
@section('content')

    <div class="col-sm-12">
        <div class="white-box">
            <div class="pull-right"><a href="{{ route('create-program') }}"><button class="btn btn-block btn-primary"><i
                            class="fa fa-plus"></i> Add program</button></a></div>
            <h3 class="box-title m-b-0">Programs </h3>
            <br><br>
            @if ($message = Session::get('msg'))
                <div class="alert alert-success alert-block mt-2"> <button type="button" class="close"
                        data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @elseif ($message = Session::get('danger'))<div class="alert alert-danger alert-block mt-2"> <button
                        type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            <div class="table-responsive">
                <table id="myTable" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Program Name</th>
                            <th>Organization</th>
                            <th>Location</th>
                            <th>Course</th>
                            <th>Graduates</th>
                            <th>Awards</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($programs as $program)
                            <tr>

                                <td>{{ $program->program_name }}</td>
                                <td>{{ $program->organization_id }}</td>
                                <td>{{ $program->location }}</td>
                                <td>{{ $program->count }}</td>
                                <td>{{ $program->graduants }}</td>
                                <td>{{ $program->awards }}</td>
                                <td>
                                    <div class="pull-left"><a href="{{ route('course', $program->id) }}"><button
                                                class="btn btn-block btn-primary ">View course</button></a> </div>
                                </td>
                                <td>
                                    <div class="pull-left"><button class="btn btn-block btn-success " data-toggle="modal"
                                            data-target="#responsive-modal{{ $program->id }}">Statistics</button>
                                    </div>
                                    <div class="pull-left" style="padding-left:20px;"><a
                                            href="{{ route('edit-program', ['id' => $program->id]) }}"><button
                                                class="btn btn-block btn-primary ">Update</button></a> </div>
                                    <div class="pull-right"><button class="btn btn-block btn-danger" data-toggle="modal"
                                            data-target=".bs-example-modal-sm{{ $program->id }}" data-toggle="modal"
                                            data-target=".bs-example-modal-sm">Delete</button></div>
                                </td>
                            </tr>
                            <!-- /.modal -->
                            <div id="responsive-modal{{ $program->id }}" class="modal fade" tabindex="-1"
                                role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">×</button>
                                            <h4 class="modal-title">Update Statistics</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route('update-statistics') }}" method="POST">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">Graduants:</label>
                                                    <input type="number" class="form-control" name="graduants"
                                                        id="recipient-name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">Awards:</label>
                                                    <input type="number" class="form-control" name="awards"
                                                        id="recipient-name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">Awards Title:</label>
                                                    <input type="text" class="form-control" name="award_title"
                                                        id="recipient-name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">Awards Image:</label>
                                                    <input type="file" class="form-control" name="image">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label"> Location:</label>
                                                    <input type="text" class="form-control" name="award_location"
                                                        id="recipient-name">
                                                </div>
                                                <input type="hidden" hidden class="form-control"
                                                    value="{{ $program->id }}" name="program_id">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success waves-effect waves-light">Save
                                                changes</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>

                                {{-- <div class="modal fade bs-example-modal-sm{{ $program->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
                                    style="display: none;">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="mySmallModalLabel">Do you want to delete this
                                                    Organization</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect"
                                                    data-dismiss="modal">No</button>
                                                <a
                                                    href="{{ route('delete-organization', ['id' => $program->id]) }}"><button
                                                        class="btn btn-danger waves-effect waves-light">YES</button></a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div> --}}

                                <!-- Button trigger modal -->

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>

@endsection
