@extends('admin_panel.layouts.app')

@section('Title','Index Page')
  @section('content')

  @extends('admin_panel.layouts.app')

@section('Title', 'Index Page')
@section('content')

    <div class="col-sm-12">

        <div class="white-box">

            <div class="pull-right"><button class="btn btn-block btn-success">Reviews </button></div>

            <div class="table-responsive">
                <table id="myTable" class="table table-striped">
                    <thead>
                        <tr>

                            <th>Title</th>
                            <th>Program reviewed</th>
                            <th>Reviewers name</th>
                            <th>Ratings</th>
                            <th>Text</th>
                            <th>Status</th>
                            <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach ($reviews as $review)
                        <tr>
                            <td>{{$review->title}}</td>
                            <td>{{$review->name}}</td>
                            <td>{{ $review->first_name }} {{ $review->last_name }} </td>
                            <td>{{ $review->rating }}  </td>
                            <td>{{ $review->text }}  </td>
                            @if ($review->activated == 0)
                            <td><span class="label label-danger">Inactive</span></td>
                            @else
                            <td><span class="label label-success">Inactive</span></td>
                            @endif
                        <td>    <div class="pull-left"><button class="btn btn-block btn-primary">View</button>
                        </div></td>
                        </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


  @endsection

