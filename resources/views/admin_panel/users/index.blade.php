@extends('admin_panel.layouts.app')

@section('Title', 'Index Page')
@section('content')

    <div class="col-sm-12">

        <div class="white-box">


            <div class="pull-right"><button class="btn btn-block btn-success">Reviewers </button></div>
            {{-- <br><div class="pull-left"><button class="btn btn-block btn-primary" data-toggle="modal" data-target="#responsive-modal">Add user</button></div> --}}
            <br><br><br>
            <div class="table-responsive">
                <table id="myTable" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            {{-- <th>Action</th> --}}
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->phone}}</td>
                            <td>
                                {{-- <div class="pull-left" ><a href="{{ route('edit-user', ['id' => $user->id]) }}"><button class="btn btn-block btn-success ">View</button></a> </div> --}}
                                {{-- <div class="pull-left" ><a href="{{route('edit-program')}}"><button class="btn btn-block btn-primary ">View</button></a> </div> --}}
                                {{-- <div class="pull-right" ><button class="btn btn-block btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm{{$user->id}}" data-toggle="modal" data-target=".bs-example-modal-sm" >Delete</button></div> --}}
                            </td>
                        </tr>

                            <div class="modal fade bs-example-modal-sm{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="mySmallModalLabel">Do you want to delete this program</h4> </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">No</button>
                                            <a href="{{ route('delete-user', ['id' => $user->id]) }}"><button  class="btn btn-danger waves-effect waves-light">YES</button></a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    {{-- <!-- /.modal -->
    <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Modal Content is Responsive</h4> </div>
                <div class="modal-body">
                    <form action="{{route('store-user')}}" method="POST">
                        @csrf
                        <div class="form-body">
                            <h3 class="box-title">users </h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">users <title></title></label>
                                        <input type="text" id="firstName" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="Enter users title">
                                        @error('title')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    </div>
                                </div>

                            </div>
                            <!--/row-->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" class="form-control @error('location') is-invalid @enderror" name="location">
                                        @error('location')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                 </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Average cost</label>
                                        <input type="number" class="form-control @error('average_cost') is-invalid @enderror" name="average_cost">
                                        @error('average_cost')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"> user duration in months</label>
                                        <input type="number" class="form-control @error('duration') is-invalid @enderror" name="duration">

                                        @error('duration')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror</div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">user Description</label>
                                        <input type="text" class="form-control @error('description') is-invalid @enderror" name="description">
                                        @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror </div>
                                </div>
                                <input type="hidden" class="form-control" name="program" value="{{$program->name}}">
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                    <button type="button" class="btn btn-default">Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div> <img src="../plugins/images/model.png" alt="default"  class="model_img img-responsive" />
    <!-- Button trigger modal --> --}}


@endsection
