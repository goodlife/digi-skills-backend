   <!-- ===== Left-Sidebar ===== -->
   <aside class="sidebar">
    <div class="scroll-sidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div class="profile-image">
                    <img src="{{asset('asset/plugins/images/users/hanna.jpg')}}" alt="user-img" class="img-circle">
                    <a href="{{route('home')}}" class="dropdown-toggle u-dropdown text-blue"
                        data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="badge badge-danger">
                            <i class="fa fa-angle-down"></i>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated flipInY">
                        <li><a href="{{route('home')}}"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="{{route('home')}}"><i class="fa fa-inbox"></i> Inbox</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{route('home')}}"><i class="fa fa-cog"></i> Account Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href=""><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                </div>
                <p class="profile-text m-t-15 font-16"><a href="{{route('home')}}"> Hanna Gover</a></p>
            </div>
        </div>
        <nav class="sidebar-nav">
            <ul id="side-menu">
                @if(Auth::user()->inRole('admin'))
                <li>
                    <a class="active waves-effect" href="{{route('home')}}" aria-expanded="false"><i
                            class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Dashboard <span
                                class="label label-rounded label-info pull-right"></span></span></a>
                </li>
                <li>
                    <a class="active waves-effect" href="{{route('organization')}}" aria-expanded="false"><i
                            class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Organization</a>
                </li>
                <li>
                    <a class="active waves-effect"href="{{route('program')}}" aria-expanded="false"><i
                            class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Programs </a>
                </li>

                <li>
                    <a class="active waves-effect"href="{{route('users')}}" aria-expanded="false"><i
                            class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Users </a>
                </li>
                <li>
                    <a class="active waves-effect"href="{{route('reviews')}}" aria-expanded="false"><i
                            class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Reviews </a>
                </li>

                @else
                <li>
                    <a class="active waves-effect"href="{{route('program')}}" aria-expanded="false"><i
                            class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Programs</a>
                </li>
                @endif

            </ul>
        </nav>
    </div>
</aside>
<!-- ===== Left-Sidebar-End ===== -->
<!-- ===== Page-Content ===== -->
<div class="page-wrapper">
