<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="asset/plugins/images/favicon.png">
    <title>DIGI SKILL</title>
    <!-- ===== Bootstrap CSS ===== -->
    <link href="{{ asset('asset/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- ===== Plugin CSS ===== -->
    <link href="{{ asset('asset/plugins/components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link
        href="{{ asset('asset/plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}"
        rel="stylesheet">
    <link href="{{ asset('asset/plugins/components/fullcalendar/fullcalendar.css') }}" rel='stylesheet'>
    <!-- ===== Animation CSS ===== -->
    <link href="{{ asset('asset/css/animate.css') }}" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="{{ asset('asset/css/style.css') }}" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
        <!-- ===== Plugin CSS ===== -->
        <link href="{{ asset('asset/plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('asset/css/colors/default.css') }}" id="theme" rel="stylesheet">
</head>

<body class="mini-sidebar">
    <!-- ===== Main-Wrapper ===== -->
    <div id="wrapper">
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <!-- ===== Top-Navigation ===== -->
