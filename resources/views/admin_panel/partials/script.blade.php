</div>
<!-- ===== Main-Wrapper-End ===== -->
<!-- ==============================
=============================== -->
<!-- ===== jQuery ===== -->
<script src="{{ asset('asset/plugins/components/jquery/dist/jquery.min.js') }}"></script>
<!-- ===== Bootstrap JavaScript ===== -->
<script src="{{ asset('asset/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- ===== Slimscroll JavaScript ===== -->
<script src="{{ asset('asset/js/jquery.slimscroll.js') }}"></script>
<!-- ===== Wave Effects JavaScript ===== -->
<script src="{{ asset('asset/js/waves.js') }}"></script>
<!-- ===== Menu Plugin JavaScript ===== -->
<script src="{{ asset('asset/js/sidebarmenu.js') }}"></script>
<!-- ===== Custom JavaScript ===== -->
<script src="{{ asset('asset/js/custom.js') }}"></script>
<!-- ===== Plugin JS ===== -->
<script src="{{ asset('asset/plugins/components/chartist-js/dist/chartist.min.js') }}"></script>
<script
    src="{{ asset('asset/plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}">
</script>
<script src="{{ asset('asset/plugins/components/moment/moment.js') }}"></script>
<script src="{{ asset('asset/plugins/components/fullcalendar/fullcalendar.js') }}""></script>
<script  src=" {{ asset('asset/js/db2.js') }}"></script>
<!-- ===== Style Switcher JS ===== -->
<script src="{{ asset('asset/plugins/components/styleswitcher/jQuery.style.switcher.js') }}"></script>

<script src="{{ asset('asset/plugins/components/datatables/jquery.dataTables.min.js') }}"></script>

 <!-- start - This is for export functionality only -->
 <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
 <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
 <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
 <!-- end - This is for export functionality only -->
 <script>
 $(function() {
     $('#myTable').DataTable();

     var table = $('#example').DataTable({
         "columnDefs": [{
             "visible": false,
             "targets": 2
         }],
         "order": [
             [2, 'asc']
         ],
         "displayLength": 25,
         "drawCallback": function(settings) {
             var api = this.api();
             var rows = api.rows({
                 page: 'current'
             }).nodes();
             var last = null;
             api.column(2, {
                 page: 'current'
             }).data().each(function(group, i) {
                 if (last !== group) {
                     $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                     last = group;
                 }
             });
         }
     });
     // Order by the grouping
     $('#example tbody').on('click', 'tr.group', function() {
         var currentOrder = table.order()[0];
         if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
             table.order([2, 'desc']).draw();
         } else {
             table.order([2, 'asc']).draw();
         }
     });
 });
 $('#example23').DataTable({
     dom: 'Bfrtip',
     buttons: [
         'copy', 'csv', 'excel', 'pdf', 'print'
     ]
 });
 </script>
</body>

</html>
