@extends('admin_panel.layouts.app')
@section('Title', 'Index Page')
@section('content')
<div class="error-box">
    <div class="error-body text-center">
        <h1>403</h1>
        <h3 class="text-uppercase">Forbidden Error</h3>
        <p class="text-muted m-t-30 m-b-30 text-uppercase">You don't have permission to access only Admin.</p>

</div>
@endsection
