@extends('admin_panel.layouts.app')

@section('Title', 'Index Page')
@section('content')
    <!-- ===== Page-Container ===== -->
    <div class="container-fluid">
        <div class="row colorbox-group-widget">
            <div class="col-md-3 col-sm-6 info-color-box">
                <div class="white-box">
                    <div class="media bg-primary">
                        <div class="media-body">
                            <h3 class="info-count">{{$organizationCount}} <span class="pull-right"><i
                                        class="mdi mdi-checkbox-marked-circle-outline"></i></span></h3>
                            <p class="info-text font-12">Organizations</p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 info-color-box">
                <div class="white-box">
                    <div class="media bg-success">
                        <div class="media-body">
                            <h3 class="info-count">{{$programCount}}  <span class="pull-right"><i
                                        class="mdi mdi-comment-text-outline"></i></span></h3>
                            <p class="info-text font-12">Programs</p>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 info-color-box">
                <div class="white-box">
                    <div class="media bg-danger">
                        <div class="media-body">
                            <h3 class="info-count">{{$coursesCount}}  <span class="pull-right"><i class="mdi mdi-coin"></i></span>
                            </h3>
                            <p class="info-text font-12">Courses</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 info-color-box">
                <div class="white-box">
                    <div class="media bg-warning">
                        <div class="media-body">
                            <h3 class="info-count"> {{$usersCount}}<span class="pull-right"><i class="mdi mdi-coin"></i></span>
                            </h3>
                            <p class="info-text font-12">Users</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="white-box sd-widget m-b-0 b-b-0">
                    <a href="javascript:void(0);" class="pull-right"><i class="icon-settings"></i></a>
                    <h4 class="box-title">Sales Difference</h4>
                </div>
                <div class="white-box p-0 b-t-0">
                    <div class="ct-sd-chart chart-pos"></div>
                    <ul class="list-inline t-a-c">
                        <li>
                            <h6 class="font-15"><i class="fa fa-circle m-r-5 text-danger"></i>March</h6>
                        </li>
                        <li>
                            <h6 class="font-15"><i class="fa fa-circle m-r-5 text-primary"></i>February</h6>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="white-box sc-widget">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="box-title">Sales Chart</h4>
                        </div>
                        <div class="col-sm-6">
                            <select class="custom-select">
                                <option selected>March 2017</option>
                                <option value="1">April 2017</option>
                                <option value="2">May 2017</option>
                                <option value="3">June 2017</option>
                            </select>
                        </div>
                    </div>
                    <div class="chartist-sales-chart chart-pos m-t-40"></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="white-box ie-widget m-b-0 b-b-0">
                    <h4 class="box-title">Item Earnings</h4>
                </div>
                <div class="white-box p-0 b-t-0">
                    <div class="ct-ie-chart chart-pos"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box user-table">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="box-title">Table Format/User Data</h4>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-inline">
                                <li>
                                    <a href="javascript:void(0);" class="btn btn-default btn-outline font-16"><i
                                            class="fa fa-trash" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="btn btn-default btn-outline font-16"><i
                                            class="fa fa-commenting" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                            <select class="custom-select">
                                <option selected>Sort by</option>
                                <option value="1">Name</option>
                                <option value="2">Location</option>
                                <option value="3">Type</option>
                                <option value="4">Role</option>
                                <option value="5">Action</option>
                            </select>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="checkbox checkbox-info">
                                            <input id="c1" type="checkbox">
                                            <label for="c1"></label>
                                        </div>
                                    </th>
                                    <th>Name</th>
                                    <th>Location</th>
                                    <th>Type</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="checkbox checkbox-info">
                                            <input id="c2" type="checkbox">
                                            <label for="c2"></label>
                                        </div>
                                    </td>
                                    <td><a href="javascript:void(0);" class="text-link">Daniel Kristeen</a></td>
                                    <td>Texas, US</td>
                                    <td>Posts 564</td>
                                    <td><span class="label label-success">Admin</span></td>
                                    <td>
                                        <select class="custom-select">
                                            <option value="1">Modulator</option>
                                            <option value="2">Admin</option>
                                            <option value="3">Staff</option>
                                            <option value="4">User</option>
                                            <option value="5">General</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="checkbox checkbox-info">
                                            <input id="c3" type="checkbox">
                                            <label for="c3"></label>
                                        </div>
                                    </td>
                                    <td><a href="javascript:void(0);" class="text-link">Hanna Gover</a></td>
                                    <td>Los Angeles, US</td>
                                    <td>Posts 451</td>
                                    <td><span class="label label-info">Staff</span> </td>
                                    <td>
                                        <select class="custom-select">
                                            <option value="1">Modulator</option>
                                            <option value="2">Admin</option>
                                            <option value="3">Staff</option>
                                            <option value="4">User</option>
                                            <option value="5">General</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="checkbox checkbox-info">
                                            <input id="c4" type="checkbox">
                                            <label for="c4"></label>
                                        </div>
                                    </td>
                                    <td><a href="javascript:void(0);" class="text-link">Jeffery Brown</a></td>
                                    <td>Houston, US</td>
                                    <td>Posts 978</td>
                                    <td><span class="label label-danger">User</span> </td>
                                    <td>
                                        <select class="custom-select">
                                            <option value="1">Modulator</option>
                                            <option value="2">Admin</option>
                                            <option value="3">Staff</option>
                                            <option value="4">User</option>
                                            <option value="5">General</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="checkbox checkbox-info">
                                            <input id="c5" type="checkbox">
                                            <label for="c5"></label>
                                        </div>
                                    </td>
                                    <td><a href="javascript:void(0);" class="text-link">Elliot Dugteren</a></td>
                                    <td>San Antonio, US</td>
                                    <td>Posts 34</td>
                                    <td><span class="label label-warning">General</span> </td>
                                    <td>
                                        <select class="custom-select">
                                            <option value="1">Modulator</option>
                                            <option value="2">Admin</option>
                                            <option value="3">Staff</option>
                                            <option value="4">User</option>
                                            <option value="5">General</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="checkbox checkbox-info">
                                            <input id="c6" type="checkbox">
                                            <label for="c6"></label>
                                        </div>
                                    </td>
                                    <td><a href="javascript:void(0);" class="text-link">Sergio Milardovich</a></td>
                                    <td>Jacksonville, US</td>
                                    <td>Posts 31</td>
                                    <td><span class="label label-primary">Partial</span> </td>
                                    <td>
                                        <select class="custom-select">
                                            <option value="1">Modulator</option>
                                            <option value="2">Admin</option>
                                            <option value="3">Staff</option>
                                            <option value="4">User</option>
                                            <option value="5">General</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <ul class="pagination">
                        <li class="disabled"> <a href="#">1</a> </li>
                        <li class="active"> <a href="#">2</a> </li>
                        <li> <a href="#">3</a> </li>
                        <li> <a href="#">4</a> </li>
                        <li> <a href="#">5</a> </li>
                    </ul>
                    <a href="javascript:void(0);" class="btn btn-success pull-right m-t-10 font-20">+</a>
                </div>
            </div>
        </div> --}}
        <!-- ===== Right-Sidebar-End ===== -->
    </div>
    <!-- ===== Page-Container-End ===== -->

@endsection
