    @include('admin_panel.partials.header')

      @include('admin_panel.partials.navigation')
      @include('admin_panel.partials.sidebar')

    @yield('content')


    @include('admin_panel.partials.footer')
    @include('admin_panel.partials.script')
