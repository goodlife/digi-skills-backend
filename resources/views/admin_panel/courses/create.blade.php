@extends('admin_panel.layouts.app')

@section('Title','Index Page')
  @section('content')
  <!--.row-->
  <br><br><br>
  <div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">Add Courses</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                     <form action="{{route('store-course')}}" method="POST">
                        @csrf
                        <div class="form-body">
                            <h3 class="box-title">Courses </h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Courses <title></title></label>
                                        <input type="text" id="firstName" name="title" class="form-control" placeholder="Enter Courses title"> </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Program</label>
                                        <select class="form-control" name="program">
                                            <option value="">Start Up</option>
                                            <option value="">Old </option>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" class="form-control" name="location"> </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Average cost</label>
                                        <input type="number" class="form-control" name="average_cost"> </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"> Course duration in months</label>
                                        <input type="number" class="form-control" name="duration"> </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Course Description</label>
                                        <input type="text" class="form-control" name="description"> </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            <button type="button" class="btn btn-default">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--./row-->

  @endsection
