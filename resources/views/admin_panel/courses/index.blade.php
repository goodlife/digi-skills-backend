@extends('admin_panel.layouts.app')

@section('Title', 'Index Page')
@section('content')

    <div class="col-sm-12">

        <div class="white-box">

            <h3 class="box-title m-b-0"><strong>Program Name:</strong>{{ $program->program_name}} </h3>
            <div class="pull-right"><a href="{{route('program')}}"><button class="btn btn-block btn-success">Complete Add Program </button></a></div>
            <br><div class="pull-left"><button class="btn btn-block btn-primary" data-toggle="modal" data-target="#responsive-modal">Add course</button></div>
            <br><br><br>
            @if ($message = Session::get('msg'))<div class="alert alert-success alert-block mt-2">	<button type="button" class="close" data-dismiss="alert">×</button>	        <strong>{{ $message }}</strong></div>@endif

            <div class="table-responsive">
                <table id="myTable" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Title</th>

                            <th>Average Cost</th>
                            <th>Location</th>
                            <th>Duration</th>
                            <th>Duration</th>
                    </thead>
                    <tbody>
                        @foreach ($courses as $course)
                        <tr>
                            <td>{{$course->title}}</td>
                            <td>{{$course->average_cost}}</td>
                            <td>{{$course->location}}</td>
                            <td>{{$course->duration}}</td>
                            <td>
                                <div class="pull-left" ><a href="{{ route('edit-course', ['id' => $course->id]) }}"><button class="btn btn-block btn-success ">View</button></a> </div>
                                {{-- <div class="pull-left" ><a href="{{route('edit-program')}}"><button class="btn btn-block btn-primary ">View</button></a> </div> --}}
                                <div class="pull-right" ><button class="btn btn-block btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm{{$course->id}}" data-toggle="modal" data-target=".bs-example-modal-sm" >Delete</button></div>
                            </td>
                        </tr>

                            <div class="modal fade bs-example-modal-sm{{$course->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="mySmallModalLabel">Do you want to delete this program</h4> </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">No</button>
                                            <a href="{{ route('delete-course', ['id' => $course->id]) }}"><button  class="btn btn-danger waves-effect waves-light">YES</button></a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- /.modal -->
    <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Modal Content is Responsive</h4> </div>
                <div class="modal-body">
                    <form action="{{route('store-course')}}" method="POST">
                        @csrf
                        <div class="form-body">
                            <h3 class="box-title">Courses </h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Courses <title></title></label>
                                        <input type="text" id="firstName" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="Enter Courses title">
                                        @error('title')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    </div>
                                </div>

                            </div>
                            <!--/row-->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" class="form-control @error('location') is-invalid @enderror" name="location">
                                        @error('location')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                 </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Average cost</label>
                                        <input type="number" class="form-control @error('average_cost') is-invalid @enderror" name="average_cost">
                                        @error('average_cost')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"> Course duration in months</label>
                                        <input type="number" class="form-control @error('duration') is-invalid @enderror" name="duration">

                                        @error('duration')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror</div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Course Description</label>
                                        <input type="text" class="form-control @error('description') is-invalid @enderror" name="description">
                                        @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror </div>
                                </div>
                                <input type="hidden" class="form-control" name="program" value="{{$program->id}}">
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                    <button type="button" class="btn btn-default">Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div> <img src="../plugins/images/model.png" alt="default"  class="model_img img-responsive" />
    <!-- Button trigger modal -->


@endsection
