@extends('admin_panel.layouts.app')

@section('Title', 'Index Page')
@section('content')

    <div class="col-sm-12">


        <div class="white-box">
            <br>
            <br>
            @if ($message = Session::get('msg'))<div class="alert alert-success alert-block mt-2">	<button type="button" class="close" data-dismiss="alert">×</button>	        <strong>{{ $message }}</strong></div>@endif

            <div class="pull-right"><a href="{{ route('create-organization') }}"><button
                        class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Organization</button></a></div>
            <h3 class="box-title m-b-0">Organization </h3>
            <br><br>
            <div class="table-responsive">
                <table id="myTable" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Country of Operation</th>
                            <th>Program length</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($organizations as $organization)
                            <tr>
                                <td>{{ $organization->name }}</td>
                                <td>{{ $organization->email }}</td>
                                <td>{{ $organization->phone }}</td>
                                <td>{{ $organization->country_of_operation }}</td>
                                <td>{{ $organization->program_duration }}</td>
                                <td>
                                    <div class="pull-left"><a
                                            href="{{ route('edit-organization', ['id' => $organization->id]) }}"><button
                                                class="btn btn-block btn-success ">View</button></a> </div>
                                    <div class="pull-left" style="padding-left:20px;"><button
                                            class="btn btn-block btn-danger" data-toggle="modal"
                                            data-target=".bs-example-modal-sm{{ $organization->id }}">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade bs-example-modal-sm{{ $organization->id }}" tabindex="-1" role="dialog"
                                aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="mySmallModalLabel">Do you want to delete this
                                                Organization</h4>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default waves-effect"
                                                data-dismiss="modal">No</button>
                                            <a href="{{ route('delete-organization', ['id' => $organization->id]) }}"><button
                                                    class="btn btn-danger waves-effect waves-light">YES</button></a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection
