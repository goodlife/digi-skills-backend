@extends('admin_panel.layouts.app')

@section('Title','Index Page')
  @section('content')
  <!--.row-->
  <br><br><br>
  <div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">Update Organization</div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    <form action="{{route('update-organization',$organization->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <h3 class="box-title">Organization Profile</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Organization Name</label>
                                        <input type="text" id="firstName" name="name" value="{{$organization->name}}" class="form-control" placeholder="Enter Organization name"> </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Category</label>
                                        <select class="form-control" name="category">
                                            <option value="start_up">Start Up</option>
                                            <option value="old">Old </option>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Year established</label>
                                        <input type="text" class="form-control" name="year_establish" value="{{$organization->year_establish}}" > </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Country of operation</label>
                                        <input type="text" class="form-control" name="country_of_operation" value="{{$organization->country_of_operation}}" > </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">About</label>
                                        <input type="text" class="form-control" name="about" value="{{$organization->about}}" > </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Program Length</label>
                                        <input type="text" class="form-control" name="program_duration" value="{{$organization->program_duration}}" > </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Description</label>
                                        <input type="textarea" class="form-control" name="description" value="{{$organization->description}}" > </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Additianal Remarks</label>
                                        <input type="textarea" class="form-control" name="additional_remarks" value="{{$organization->additional_remarks}}" > </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Logo</label>
                                        <input type="file" class="form-control" name="image"> </div>
                                </div>

                                <!--/span-->
                            </div>
                            <!--/row-->
                            <h3 class="box-title m-t-40">Address</h3>
                            <hr>

                            <div class="row">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Phone </label>
                                        <input type="text" class="form-control" name="phone" value="{{$organization->phone}}" > </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" class="form-control" name="email" value="{{$organization->email}}" > </div>
                                </div>
                                <!--/span-->
                            </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Postal Code</label>
                                            <input type="text" class="form-control" name="postal_code" value="{{$organization->postal_code}}" > </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Current address</label>
                                            <input type="text" class="form-control" name="address" value="{{$organization->address}}" > </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Website</label>
                                            <input type="text" class="form-control" name="website" value="{{$organization->website}}" > </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">City</label>
                                            <input type="text" class="form-control" name="city" value="{{$organization->city}}" > </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->

                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            <button type="button" class="btn btn-default">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--./row-->

  @endsection
