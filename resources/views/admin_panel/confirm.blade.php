<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>DIGI-SKILLS </title>
    <!-- ===== Bootstrap CSS ===== -->

    <link href="{{ asset('asset/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- ===== Animation CSS ===== -->
    <link href="{{ asset('asset/css/animate.css') }}" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="{{ asset('asset/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('asset/css/colors/default.css') }}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="mini-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form class="form-horizontal form-material" id="loginform" method="post" action="{{route('confirm-verification')}}">
                    @csrf
                    <h3 class="box-title m-b-20"> Check your email for code</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="code" required="" placeholder="Enter code sent to your email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Confirm Email</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
<!-- ===== jQuery ===== -->
<script src="{{ asset('asset/plugins/components/jquery/dist/jquery.min.js') }}"></script>
<!-- ===== Bootstrap JavaScript ===== -->
<script src="{{ asset('asset/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- ===== Slimscroll JavaScript ===== -->
<script src="{{ asset('asset/js/jquery.slimscroll.js') }}"></script>
<!-- ===== Wave Effects JavaScript ===== -->
<script src="{{ asset('asset/js/waves.js') }}"></script>
<!-- ===== Menu Plugin JavaScript ===== -->
<script src="{{ asset('asset/js/sidebarmenu.js') }}"></script>
<!-- ===== Custom JavaScript ===== -->
<script src="{{ asset('asset/js/custom.js') }}"></script>
<!-- ===== Plugin JS ===== -->
<script src="{{ asset('asset/plugins/components/chartist-js/dist/chartist.min.js') }}"></script>
<script
    src="{{ asset('asset/plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}">
</script>
<script src="{{ asset('asset/plugins/components/moment/moment.js') }}"></script>
<script src="{{ asset('asset/plugins/components/fullcalendar/fullcalendar.js') }}""></script>
<script  src=" {{ asset('asset/js/db2.js') }}"></script>
<!-- ===== Style Switcher JS ===== -->
<script src="{{ asset('asset/plugins/components/styleswitcher/jQuery.style.switcher.js') }}"></script>
</body>

</html>
