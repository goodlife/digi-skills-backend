<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\EmailVerifaction;
use App\Models\Organization;
use App\Models\Programs;
use App\Models\Courses;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     return view('home');
    // }
    public function index()
    {
        $user= Auth::user()->first();

        $users = User::all();
        $organizationCount = Organization::count();
        $programCount =Programs::count();
        $coursesCount =Courses::count();
        $usersCount =User::count();
        return view('admin_panel.dashboard',compact('users','user','organizationCount','programCount','coursesCount','usersCount'));

    }

    public function confirm()
    {
        $user= Auth::user()->first();

        $users = User::all();

        return view('admin_panel.confirm',compact('users','user'));
    }


    public function confirmVerification(Request $request)
    {

        $code = EmailVerifaction::where('code',$request->code)->first();

        if ($code) {
        return  redirect(route('home'));
        } else {
            return redirect()->back();
        }
    }


    public function Users(){

        $users=User::all();

        return view('admin_panel.users.index',compact('users'));
    }




}
