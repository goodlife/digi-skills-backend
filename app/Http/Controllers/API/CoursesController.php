<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Courses;
use Illuminate\Http\Request;
use DB;
use App\Models\Programs;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        // dd($id);
        $program = Programs::where('id',$id)->first();
        $courses = Courses::all()->where('program',$id);
        $program = Programs::find($id);
        return [
            'courses' => $courses,
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin_panel.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'average_cost' => 'required',
            'duration' => 'required',
            'description' => 'required',
            'program' => 'required',
            'location' => 'required',

        ]);


                $formInput['title']=$request->title;
                $formInput['average_cost']=$request->average_cost;
                $formInput['location']=$request->location;
                $formInput['duration']=$request->duration;
                $formInput['description']=$request->description;
                $formInput['program']=$request->program;

           $course = Courses::create($formInput);

           return [
            'message'=>'Your Course has been saved successfully',
            'course' => $course
           ];

        }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function show(Courses $courses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $course = Courses::find($id);
        return [
            'course'=>$course,
        ];
       }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $formInput['title']=$request->title;
        $formInput['average_cost']=$request->average_cost;
        $formInput['location']=$request->location;
        $formInput['duration']=$request->duration;
        $formInput['description']=$request->description;
        $formInput['program']=1;
      $course=  DB::table('courses')->where('id', $id)->update($formInput);

        return [
            'courses'=>$courses,
            'message'=>'Your courses has been saved successfully'
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleteData=Courses::findorFail($id);
        $deleteData->delete();
        return redirect()->back();
    }
}
