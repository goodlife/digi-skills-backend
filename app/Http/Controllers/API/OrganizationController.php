<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use Illuminate\Http\Request;
use DB;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $organizations = Organization::all();
        return [
            'organizations' => $organizations,
        ];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        {
            //
            return view('admin_panel.organizations.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'website' => 'required',
            'address' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
            'category' => 'required',
            'year_establish' => 'required',
            'country_of_operation' => 'required',
            'about' => 'required',
            'program_duration' => 'required',
            'description' => 'required',

        ]);

            $image=$request->file('image');
            if($image){
                $imageName=$image->getClientOriginalName();
                $destination = base_path() . '/public/images';
                $image->move($destination,$imageName);


                $formInput['logo']=$imageName;
                $formInput['name']=$request->name;
                $formInput['phone']=$request->phone;
                $formInput['email']=$request->email;
                $formInput['website']=$request->website;
                $formInput['address']=$request->address;
                $formInput['city']=$request->city;
                $formInput['postal_code']=$request->postal_code;
                $formInput['category']=$request->category;
                $formInput['year_establish']=$request->year_establish;
                $formInput['country_of_operation']=$request->country_of_operation;
                $formInput['about']=$request->about;
                $formInput['program_duration']=$request->program_duration;
                $formInput['description']=$request->description;
                $formInput['additional_remarks']=$request->additional_remarks;
            }
         $organization = Organization::create($formInput);



            $profile['profile_status']= 1;
         $updateProfile =  DB::table('users')->where('email',$request->email)->update($profile);


        return [
            'organization' =>$organization
         ];

        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $organization = Organization::find($id);

        return [
            'organization' =>$organization
         ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $image=$request->file('image');
        if($image){
            $imageName=$image->getClientOriginalName();
            $destination = base_path() . '/public/images';
            $image->move($destination,$imageName);


            $formInput['logo']=$imageName;
            $formInput['name']=$request->name;
            $formInput['phone']=$request->phone;
            $formInput['email']=$request->email;
            $formInput['website']=$request->website;
            $formInput['address']=$request->address;
            $formInput['city']=$request->city;
            $formInput['postal_code']=$request->postal_code;
            $formInput['category']=$request->category;
            $formInput['year_establish']=$request->year_establish;
            $formInput['country_of_operation']=$request->country_of_operation;
            $formInput['about']=$request->about;
            $formInput['program_duration']=$request->program_duration;
            $formInput['description']=$request->description;
            $formInput['additional_remarks']=$request->additional_remarks;

        }

        $organization=   DB::table('organizations')->where('id', $id)->update($formInput);
        return [
            'organization' =>$organization
         ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleteData=Organization::findorFail($id);
        $deleteData->delete();
        return redirect()->back();
    }
}
