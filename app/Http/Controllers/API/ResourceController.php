<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Resource;
use Illuminate\Http\Request;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       $resource = Resource::all();

       return [
        'resource' => $resource,
    ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //'title','author_name','institution','text','date'

        $validatedData = $request->validate([
            'title' => 'required',
            'author_name' => 'required',
            'institution' => 'required',
            'text' => 'required',
            'date' => 'required',


        ]);

                $formInput['title']=$request->title;
                $formInput['author_name']=$request->author_name;
                $formInput['institution']=$request->institution;
                $formInput['text']=$request->text;
                $formInput['date']=$request->date;

               $resource = Resource::create($formInput);


                return [
                    'message'=>'Your resource has been saved successfully',
                    'course' => $resource
                   ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function show(Resource $resource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function edit(Resource $resource)
    {
        //
        $resource = Resource::find($id);

             return view('admin_panel.courses.edit', compact('resource'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resource $resource)
    {
        //
        $formInput['title']=$request->title;
        $formInput['author_name']=$request->author_name;
        $formInput['location']=$request->location;
        $formInput['institution']=$request->institution;
        $formInput['text']=$request->text;
        $formInput['date']=$request->date;
        $resource  = DB::table('resources')->where('id', $id)->update($formInput);
        return [
            'message'=>'Your resource has been update successfully',
            'resource' => $resource
           ];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resource $resource)
    {
        //
    }
}
