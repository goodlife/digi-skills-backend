<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone_number' => 'required'
        ]);

        $user = auth()->guard('api')->user();
        $user->update([
            'name' => $request->name,
            'phone_number' => $request->phone_number
        ]);

        return [
            'user' => [
                'first_name' => explode(' ', $user->name)[0],
                'name' => $user->name,
                'email' => $user->email,
                'phone_number' => $user->phone_number,

            ]
        ];
    }
}
