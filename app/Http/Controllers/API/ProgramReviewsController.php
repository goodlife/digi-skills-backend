<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Programs;
use App\Models\ProgramReviews;
use Illuminate\Http\Request;

class ProgramReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        $reviews = ProgramReviews::where('program_id',$id)->get();
        return [

            'reviews' => $reviews
           ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //     'title','first_name','last_name','age','review','program_id','gender','citizen','email','citizen','rating'


        $image=$request->file('image');
        // return $image;

            if($image){
                $imageName= $image->getClientOriginalName();

                $destination = base_path() . '/public/images';
             $name = $destination.'/'.$imageName;
                $image->move($destination,$imageName);

    $type= pathinfo(   $name,PATHINFO_EXTENSION);
    $data = file_get_contents(  $name);

    $base64 = 'data:image/'.$type.'base64,' . base64_encode($data);

                 $formInput['image']=$base64 ;
                $formInput['title']=$request->title;
                $formInput['first_name']=$request->first_name;
                $formInput['last_name']=$request->last_name;
                $formInput['age']=$request->age;
                $formInput['review']=$request->review;
                $formInput['program_id']=$request->program_id;
                $formInput['gender']=$request->gender;
                $formInput['citizen']=$request->citizen;
                $formInput['email']=$request->email;
                $formInput['rating']=$request->rating;

           $reviews = ProgramReviews::create($formInput);

           return [
            'message'=>'Your review has been saved successfully',
            'review' => $reviews
           ];

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProgramReviews  $programReviews
     * @return \Illuminate\Http\Response
     */
    public function show(ProgramReviews $programReviews)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProgramReviews  $programReviews
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgramReviews $programReviews)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProgramReviews  $programReviews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramReviews $programReviews)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProgramReviews  $programReviews
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgramReviews $programReviews)
    {
        //
    }

    public function updateStatus(Request $request, $id){


        $formInput['active']=$request->active;


        DB::table('program_reviews')->where('id', $request->program_id)->update($formInput);

        return [
            'msg'=>'Your status has been saved successfully'  ];
    }

}
