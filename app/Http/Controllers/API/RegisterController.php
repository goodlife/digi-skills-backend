<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\EmailVerifaction;
use App\Models\Role;
use App\Models\RoleUsers;
 use App\Http\Traits\SendSMS;
 use Validator;
 use Illuminate\Support\Str;



class RegisterController extends Controller
{

    use SendSMS;


    public function register (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'phone'=>'required',
            'user_type'=>'required'
        ]);
        if ($validator->fails())
        {
            return response(['message'=>$validator->errors()->all()], 500);
        }
        $email = User::where('email', $request->email)->first();
        $phone = User::where('phone',$request->phone)->first();
        if($email){
            return response(['message'=>'Email already in use'], 500);
        }

        if($phone){
            return response(['message'=>'Phone Number is already taken'], 500);
        }
        $code = substr(str_shuffle("0123456789"), 0,4);

        $this->sendSms($request->phone,$code);

               $verify = EmailVerifaction::create([
            'code' => $code,
            'email' => $request->email,
        ]);

        $password=Hash::make($request['password']);
        $remember_token= Str::random(10);
        $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $password,
                'user_type'=>$request->user_type,
                'phone'=>$request->phone,
                'remember_token'=>$remember_token,
                'profile_status'=>0


            ]);
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['access_token' => $token,
    'user'=> $user

];
        return response($response, 200);
    }


    public function emailVerification($code){

        $code = EmailVerifaction::where('code',$code)->first();

        if ($code) {
            return response(['message'=>'Code has been verified succesfully'], 200);
        } else {
            return response(['message'=>'Your code is incorect'], 500);
        }

    }



    protected function login($data)
    {
    	$request = Request::create('/api/v1/login', 'POST', [
            'email' => $data['email'],
            'password' => $data['password']
        ]);

        $request->headers->set('Origin', '*');

        return app()->handle($request);
    }


    public function codeVerification(Request $request)
    {

        $code = EmailVerifaction::where('code',$request->code)->first();

        if ($code) {

                return response(['message'=>'Code has been verified succesfully'], 200);

        } else {

                return response(['message'=>'Your code is incorect'], 500);

        }
    }

}
