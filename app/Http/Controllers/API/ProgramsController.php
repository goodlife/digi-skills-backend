<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use App\Models\Programs;
use App\Models\Courses;
use App\Models\ProgramReviews;

use Illuminate\Http\Request;
use DB;
use Auth;

class ProgramsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        $programs = Programs::all();
        return [
            'programs' => $programs,
        ];

    }

    public function dashboard()
    {


        $programs = Programs::select('program_name','awards','graduants','availabilty',DB::raw("DATE_FORMAT(created_at, '%Y') as date"))->get();
        $group = array();

        foreach ( $programs as $value ) {
            $group[$value['date']][] = $value;
        }
        return $group;

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $organization = Organization::all();
        return view('admin_panel.programs.create',compact('organization'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user()->id;
        $validatedData = $request->validate([
            'name' => 'required',
            'organization_id' => 'required',
            'type' => 'required',
            'availabilty' => 'required',
            'location' => 'required',
            'age_requirements' => 'required',
            'partcipation_guidelines' => 'required',
            'qualification' => 'required',
            'exprience' => 'required',
            'about' => 'required',
            'application_details' => 'required',

        ]);

            $image=$request->file('image');
            $banner_image=$request->file('banner_image');

        if($image) {


            //  $bannerImage=$banner_image->getClientOriginalName();
            //  $destination = base_path() . '/public/images';
            //  $banner_image->move($destination,$bannerImage);

                $imageName=$image->getClientOriginalName();
                $destination = base_path() . '/public/images';
                $image->move($destination,$imageName);

                $formInput['image']=$imageName;
                $formInput['banner_image']="";
                $formInput['organization_id']=$request->organization_id;
                $formInput['program_name']=$request->name;
                $formInput['type']=$request->type;
                $formInput['availabilty']=$request->availabilty;
                $formInput['location']=$request->location;
                $formInput['graduants']=$request->graduants;
                $formInput['awards']=$request->awards;
                $formInput['age_requirements']=$request->age_requirements;
                $formInput['partcipation_guidelines']=$request->partcipation_guidelines;
                $formInput['qualification']=$request->qualification;
                // $formInput['created_by']=$user;
                $formInput['about']=$request->about;
                $formInput['exprience']=$request->exprience;
                $formInput['application_details']=$request->application_details;
                $formInput['additional_requirements']=$request->additional_requirements;

        }
          $program = Programs::create($formInput);

        return [
           'program' =>$program
        ];

        }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Programs  $programs
     * @return \Illuminate\Http\Response
     */
    public function show(Programs $programs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Programs  $programs
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // //

        $program = Programs::find($id);
        if ($program) {
            # code...
            $review_count =ProgramReviews::where('program_id', $id)->count();
            $organizations =Organization::where('id',$program->organization_id)->get();
            $courses =    Courses::where('program', $id)->get();

            return [
                'program' => $program,
                'organizations'=>$organizations,
                'review_count'=>$review_count,
                'courses' =>  $courses
            ];
        }else{
            return response(['message'=>'Program does not exist'], 500);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Programs  $programs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $image=$request->file('image');
        $banner_image =$request->file('banner_image');

        if($image && $banner_image ) {

         $bannerImage=$banner_image->getClientOriginalName();
         $destination = base_path() . '/public/images';
         $banner_image->move($destination,$bannerImage);

            $imageName=$image->getClientOriginalName();
            $destination = base_path() . '/public/images';
            $image->move($destination,$imageName);


            $formInput['image']=$imageName;
            $formInput['banner_image']=$bannerImage;
            $formInput['organization_id']=$request->organization_id;
            $formInput['program_name']=$request->name;
            $formInput['type']=$request->type;
            $formInput['availabilty']=$request->availabilty;
            $formInput['location']=$request->location;
            $formInput['graduants']=$request->graduants;
            $formInput['awards']=$request->awards;
            $formInput['featured']=$request->featured;
            $formInput['activated']=$request->activated;
            $formInput['age_requirements']=$request->age_requirements;
            $formInput['partcipation_guidelines']=$request->partcipation_guidelines;
            $formInput['qualification']=$request->qualification;
            $formInput['about']=$request->about;
            $formInput['created_by']="1";
            $formInput['exprience']=$request->exprience;
            $formInput['application_details']=$request->application_details;
            $formInput['additional_requirements']=$request->additional_requirements;

        }else{
            return redirect()->with('msg','Upload images');

        }

        DB::table('programs')->where('id', $id)->update($formInput);
        return redirect(route('program'))->with('msg','Your program has been saved successfully');
    }

    public function updateStatics(Request $request){

        $formInput['graduants']=$request->graduants;
        $formInput['awards']=$request->awards;


        DB::table('programs')->where('id', $request->program_id)->update($formInput);

        return [
            'msg'=>'Your program has been saved successfully'  ];
    }


    public function updateStatus(Request $request){

        $formInput['featured']=$request->featured;
        $formInput['activated']=$request->active;


        DB::table('programs')->where('id', $request->program_id)->update($formInput);

        return [
            'msg'=>'Your status has been saved successfully'  ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Programs  $programs
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleteData=Programs::findorFail($id);
        $deleteData->delete();
        return redirect()->back();
    }


    public function awards($id)
    {
        //
        $awards=Award::findorFail($id);
        return [
            'awards'=> $awards  ];
    }
}
