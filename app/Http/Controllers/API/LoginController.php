<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use App\Http\Traits\SendSMS;
use App\Models\User;
use Validator;
use Hash;


class LoginController extends Controller
{

    use SendSMS;

    public function login (Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|',
        ]);
        if ($validator->fails())
        {
            return response(['message'=>$validator->errors()->all()], 500);
        }
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['access_token' => $token,
                              'user'=>$user

            ];
                return response($response, 200);
            } else {
                $response = ["message" => "You have entered  an incorrect password"];
                return response($response, 500);
            }
        } else {
            $response = ["message" =>'User email does not exist'];
            return response($response, 500);
        }
    }

    public function logout()
    {
        auth()->guard('api')->user()->token()->revoke();

        return 1;
    }
}
