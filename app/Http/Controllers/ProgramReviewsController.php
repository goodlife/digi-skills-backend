<?php

namespace App\Http\Controllers;

use App\Models\ProgramReviews;
use Illuminate\Http\Request;

class ProgramReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reviews = ProgramReviews::join('programs', 'programs.id','=','program_reviews.program_id')->get();

        // dd( $reviews );
        return view('admin_panel.users.reviews',compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProgramReviews  $programReviews
     * @return \Illuminate\Http\Response
     */
    public function show(ProgramReviews $programReviews)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProgramReviews  $programReviews
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgramReviews $programReviews)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProgramReviews  $programReviews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramReviews $programReviews)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProgramReviews  $programReviews
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgramReviews $programReviews)
    {
        //
    }
}
