<?php

namespace App\Http\Controllers;
use App\Models\Organization;
use App\Models\Programs;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Award;

class ProgramsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        if (Auth::user()->inRole('admin')) {
            # code...
            $programs = Programs::all();
//             $programs = Programs::select( 'program_name','programs.location as location','programs.id as program_id','organization_id','type','graduants','awards',DB::raw("count(courses.title) as count"))
//             ->join('courses','courses.program','=','programs.id')
//             // ->groupBy('program_name')
//             ->get();

//dd(  $programs);
        } else {
            $user = Auth::user()->id;
            $programs = Programs::select('program_name','programs.location as location','programs.id as program_id','organization_id','type','graduants','awards',DB::raw("count(courses.title) as count"))
            ->join('courses','courses.program','=','programs.program_name')
            ->groupBy('program_name')
            ->where('created_by',$user)
            ->get();
        }

        return view('admin_panel.programs.index',compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $organization = Organization::all();
        return view('admin_panel.programs.create',compact('organization'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $user = Auth::user()->id;
        $validatedData = $request->validate([
            'name' => 'required',
            'organization_id' => 'required',
            'type' => 'required',
            'availabilty' => 'required',
            'location' => 'required',
            'age_requirements' => 'required',
            'partcipation_guidelines' => 'required',
            'qualification' => 'required',
            'exprience' => 'required',
            'about' => 'required',
            'application_details' => 'required',
            'additional_requirements' => 'required',
        ]);

            $image =$request->file('image');
            $banner_image=$request->file('banner_image');

        if($image && $banner_image ) {


        $imageName= $image->getClientOriginalName();

        $destination = base_path() . '/public/images';
         $name = $destination.'/'.$imageName;
        $image->move($destination,$imageName);

        $type= pathinfo(   $name,PATHINFO_EXTENSION);
        $data = file_get_contents(  $name);

        $base64imageName = 'data:image/'.$type.'base64,' . base64_encode($data);

// dd( $base64imageName);

        $bannerImageName= $banner_image->getClientOriginalName();

        $destination = base_path() . '/public/images';
         $name1 = $destination.'/'.$bannerImageName;
         $banner_image->move($destination,$bannerImageName);

        $type1= pathinfo(   $name1,PATHINFO_EXTENSION);
        $data1 = file_get_contents(  $name1);

        $base64bannerImage = 'data:image/'.$type1.'base64,' . base64_encode($data1);

// dd($base64bannerImage);

                $formInput['image']=$base64imageName;
                $formInput['banner_image']=$base64bannerImage;
                $formInput['organization_id']=$request->organization_id;
                $formInput['program_name']=$request->name;
                $formInput['type']=$request->type;
                $formInput['availabilty']=$request->availabilty;
                $formInput['location']=$request->location;
                $formInput['graduants']=$request->graduants;
                $formInput['awards']=$request->awards;
                $formInput['age_requirements']=$request->age_requirements;
                $formInput['partcipation_guidelines']=$request->partcipation_guidelines;
                $formInput['qualification']=$request->qualification;
                $formInput['created_by']=$user;
                $formInput['about']=$request->about;
                $formInput['exprience']=$request->exprience;
                $formInput['application_details']=$request->application_details;
                $formInput['additional_requirements']=$request->additional_requirements;

        }else{
            return redirect()->back()->with('msg','Upload images');

        }
          $program = Programs::create($formInput);

        //   dd( $program);

            return redirect(route('course',$program->id))->with('msg','Your have created a program successfully.Please add courses');
        }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Programs  $programs
     * @return \Illuminate\Http\Response
     */
    public function show(Programs $programs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Programs  $programs
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $program = Programs::find($id);

        //  dd($program);
        $organization = Organization::all();

             return view('admin_panel.programs.edit', compact('program','organization'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Programs  $programs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $image=$request->file('image');
        $banner_image =$request->file('banner_image');

        if($image && $banner_image ) {

         $bannerImage=$banner_image->getClientOriginalName();
         $destination = base_path() . '/public/images';
         $banner_image->move($destination,$bannerImage);

            $imageName=$image->getClientOriginalName();
            $destination = base_path() . '/public/images';
            $image->move($destination,$imageName);


            $formInput['image']=$imageName;
            $formInput['banner_image']=$bannerImage;
            $formInput['organization_id']=$request->organization_id;
            $formInput['program_name']=$request->name;
            $formInput['type']=$request->type;
            $formInput['availabilty']=$request->availabilty;
            $formInput['location']=$request->location;
            $formInput['graduants']=$request->graduants;
            $formInput['awards']=$request->awards;
            $formInput['featured']=$request->featured;
            $formInput['activated']=$request->activated;
            $formInput['age_requirements']=$request->age_requirements;
            $formInput['partcipation_guidelines']=$request->partcipation_guidelines;
            $formInput['qualification']=$request->qualification;
            $formInput['about']=$request->about;
            $formInput['exprience']=$request->exprience;
            $formInput['application_details']=$request->application_details;
            $formInput['additional_requirements']=$request->additional_requirements;

        }else{
            return redirect()->back()->with('msg','Upload images');

        }

        DB::table('programs')->where('id', $id)->update($formInput);
        return redirect(route('program'))->with('msg','Your program has been updated successfully');
    }

    public function updateStatics(Request $request){


        $formInput['graduants']=$request->graduants;
        $formInput['awards']=$request->awards;

        $image=$request->file('image');
//         if($image){

//             $imageName= $image->getClientOriginalName();

//             $destination = base_path() . '/public/images';
//          $name = $destination.'/'.$imageName;
//             $image->move($destination,$imageName);

// $type= pathinfo(   $name,PATHINFO_EXTENSION);
// $data = file_get_contents(  $name);

// $base64 = 'data:image/'.$type.'base64,' . base64_encode($data);

        $awardInput['award_title']=$request->award_title;
        $awardInput['award_image']=$request->image;
        $awardInput['award_location']=$request->award_location;
        $awardInput['program_id']=$request->program_id;


       Award::create($awardInput);


      DB::table('programs')->where('id', $request->program_id)->update($formInput);
        // }

        return redirect(route('program'))->with('msg','Your program statistics has been saved successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Programs  $programs
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleteData=Programs::findorFail($id);
        $deleteData->delete();
        return redirect()->back()->with('danger','Your program was deleted succesfully');;
    }
}
