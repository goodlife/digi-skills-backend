<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use Illuminate\Http\Request;
use DB;
use App\Models\Programs;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        //  dd($id);
        $program = Programs::where('id',$id)->first();

        $courses = Courses::all()->where('program',$program->id);
        $program = Programs::find($id);
        return view('admin_panel.courses.index',compact('courses','program'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin_panel.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      //  dd($request->all());
        $validatedData = $request->validate([
            'title' => 'required',
            'average_cost' => 'required',
            'duration' => 'required',
            'description' => 'required',
            'program' => 'required',
            'location' => 'required',

        ]);

                $formInput['title']=$request->title;
                $formInput['average_cost']=$request->average_cost;
                $formInput['location']=$request->location;
                $formInput['duration']=$request->duration;
                $formInput['description']=$request->description;
                $formInput['program']=$request->program;

            Courses::create($formInput);


        $program = Programs::where('name',$request->program);

            return redirect()->back()->with('msg','Your Course has been saved successfully');
        }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function show(Courses $courses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $course = Courses::find($id);
        $program = Programs::all();

             return view('admin_panel.courses.edit', compact('course','program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $formInput['title']=$request->title;
        $formInput['average_cost']=$request->average_cost;
        $formInput['location']=$request->location;
        $formInput['duration']=$request->duration;
        $formInput['description']=$request->description;
        $formInput['program']=$request->program_id;
        DB::table('courses')->where('id', $id)->update($formInput);
        return redirect()->back()->with('msg','Your courses has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleteData=Courses::findorFail($id);
        $deleteData->delete();
        return redirect()->back()->with('danger','Your courses has been deleted successfully');
    }
}
