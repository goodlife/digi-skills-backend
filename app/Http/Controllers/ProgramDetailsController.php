<?php

namespace App\Http\Controllers;

use App\Models\ProgramDetails;
use Illuminate\Http\Request;

class ProgramDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProgramDetails  $programDetails
     * @return \Illuminate\Http\Response
     */
    public function show(ProgramDetails $programDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProgramDetails  $programDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgramDetails $programDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProgramDetails  $programDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramDetails $programDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProgramDetails  $programDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgramDetails $programDetails)
    {
        //
    }
}
