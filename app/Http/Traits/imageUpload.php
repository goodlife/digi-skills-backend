
<?php
namespace App\Http\Traits;


trait imageUpload{

    public function uploadImage($image){

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://dev-backend-api-6wvq2nbvea-uc.a.run.app/upload',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => array($image=> new CURLFILE('/path/to/file')),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return [
            'response'  => $response

        ]


    }

}


?>
