<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    use HasFactory;
    protected $fillable =[
        'name','phone','email','website','address','city','postal_code',
        'category','year_establish','country_of_operation',
        'logo','about','program_duration','description','additional_remarks'
        ];
}
