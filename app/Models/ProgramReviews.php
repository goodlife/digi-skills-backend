<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramReviews extends Model
{
    use HasFactory;
    protected $fillable =[
        'title','first_name','last_name','age',
        'review','program_id','gender','citizen','email',
        'citizen','rating','activated','image'
        ];
}
