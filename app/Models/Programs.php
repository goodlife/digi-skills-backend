<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Programs extends Model
{
    use HasFactory;
    protected $fillable =[
        'program_name','organization_id','image','banner_image','type','availabilty','location','about','graduants','awards','age_requirements','partcipation_guidelines','qualification','exprience','application_details','additional_requirements','created_by'
        ];
    }
