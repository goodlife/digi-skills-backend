<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailVerifaction extends Model
{
    protected $fillable = [
        'email', 'code', 'verified_at'
    ];
}
