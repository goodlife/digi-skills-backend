<?php

return [

    /*
    |--------------------------------------------------------------------------
    | The Application's roles
    |--------------------------------------------------------------------------
    |
    | Hee you can define the different roles fo this application. A role
    | has permissions which dictate what action a user can accomplish
    | upon a certain resource.
    |
    */

    'roles' => [
        'administrator' => [
            'name' => 'Admin',
            'slug' => 'admin',
            'permissions' => [
                'create' => true,
                'update' => true,
            ],
        ],
        'organization' => [
            'name' => 'Organization',
            'slug' => 'organization',
            'permissions' => [
                'create-product' => true,
                'update-product' => true,
            ],
        ],
        'user' => [
            'name' => 'User',
            'slug' => 'user',
            'permissions' => [
                'create-product' => true,
                'update-product' => true,
            ],
        ],
    ],
];
