<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/confirm', [App\Http\Controllers\HomeController::class, 'confirm'])->name('confirm');
Route::post('/confirm-verification', [App\Http\Controllers\HomeController::class, 'confirmVerification'])->name('confirm-verification');
Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/users', [App\Http\Controllers\HomeController::class, 'users'])->name('users');
Route::get('/edit-users/{id}', [App\Http\Controllers\HomeController::class, 'edit'])->name('edit-user');
Route::post('/update-users/{id}', [App\Http\Controllers\HomeController::class, 'update'])->name('update-user');
Route::get('/delete-users/{id}', [App\Http\Controllers\HomeController::class, 'destroy'])->name('delete-user');





//organization
Route::get('/organization', [App\Http\Controllers\OrganizationController::class, 'index'])->name('organization');
Route::get('/create-organization', [App\Http\Controllers\OrganizationController::class, 'create'])->name('create-organization');
Route::post('/store-organization', [App\Http\Controllers\OrganizationController::class, 'store'])->name('store-organization');
Route::get('/edit-organization/{id}', [App\Http\Controllers\OrganizationController::class, 'edit'])->name('edit-organization');
Route::post('/update-organization/{id}', [App\Http\Controllers\OrganizationController::class, 'update'])->name('update-organization');
Route::get('/delete-organization/{id}', [App\Http\Controllers\OrganizationController::class, 'destroy'])->name('delete-organization');


//programs
Route::get('/program', [App\Http\Controllers\ProgramsController::class, 'index'])->name('program');
Route::get('/create-program', [App\Http\Controllers\ProgramsController::class, 'create'])->name('create-program');
Route::post('/store-program', [App\Http\Controllers\ProgramsController::class, 'store'])->name('store-program');
Route::get('/edit-program/{id}', [App\Http\Controllers\ProgramsController::class, 'edit'])->name('edit-program');
Route::post('/update-program/{id}', [App\Http\Controllers\ProgramsController::class, 'update'])->name('update-program');
Route::get('/delete-program/{id}', [App\Http\Controllers\ProgramsController::class, 'destroy'])->name('delete-program');
Route::post('/update-statistics', [App\Http\Controllers\ProgramsController::class, 'updateStatics'])->name('update-statistics');


Route::get('/reviews', [App\Http\Controllers\ProgramReviewsController::class, 'index'])->name('reviews');




Route::get('/course/{id}', [App\Http\Controllers\CoursesController::class, 'index'])->name('course');
Route::get('/create-course', [App\Http\Controllers\CoursesController::class, 'create'])->name('create-course');
Route::post('/store-course', [App\Http\Controllers\CoursesController::class, 'store'])->name('store-course');
Route::get('/edit-course/{id}', [App\Http\Controllers\CoursesController::class, 'edit'])->name('edit-course');
Route::post('/update-course/{id}', [App\Http\Controllers\CoursesController::class, 'update'])->name('update-course');
Route::get('/delete-course/{id}', [App\Http\Controllers\CoursesController::class, 'destroy'])->name('delete-course');




