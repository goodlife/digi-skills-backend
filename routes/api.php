<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['prefix' => 'v1', 'namespace' => 'API'], function () {
  Route::get('/init', 'InitController@init');


  Route::post('/register', [ App\Http\Controllers\API\RegisterController::class, 'register']);
  Route::post('/login', [ App\Http\Controllers\API\LoginController::class, 'login']);
  Route::post('/confirm', [ App\Http\Controllers\API\RegisterController::class, 'codeVerification']);

  Route::group(['middleware' =>'auth:api'], function() {

    Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);


    Route::get('/organizations', [App\Http\Controllers\API\OrganizationController::class, 'index']);
    Route::post('/organizations', [App\Http\Controllers\API\OrganizationController::class, 'store']);
    Route::get('/organizations/{id}', [App\Http\Controllers\API\OrganizationController::class, 'edit']);
    Route::post('/organizations/{id}', [App\Http\Controllers\API\OrganizationController::class, 'update']);

    Route::get('/programs', [App\Http\Controllers\API\ProgramsController::class, 'index']);
    Route::post('/programs', [App\Http\Controllers\API\ProgramsController::class, 'store']);
    Route::get('/programs/{id}', [App\Http\Controllers\API\ProgramsController::class, 'edit']);
    Route::post('/programs{id}', [App\Http\Controllers\API\ProgramsController::class, 'update']);

    Route::post('/update-stats', [App\Http\Controllers\API\ProgramsController::class, 'updateStatics']);
    Route::post('/update-stattus', [App\Http\Controllers\API\ProgramsController::class, 'updateStatus']);

    Route::get('/awards/{id}', [App\Http\Controllers\API\ProgramsController::class, 'awards']);


    Route::get('dashboard', [App\Http\Controllers\API\ProgramsController::class, 'dashboard']);



Route::get('/course/{id}', [App\Http\Controllers\API\CoursesController::class, 'index'])->name('course');
Route::post('/course', [App\Http\Controllers\API\CoursesController::class, 'store'])->name('store-course');
Route::get('/edit-course/{id}', [App\Http\Controllers\API\CoursesController::class, 'edit'])->name('edit-course');
Route::post('/update-course/{id}', [App\Http\Controllers\API\CoursesController::class, 'update'])->name('update-course');
Route::get('/delete-course/{id}', [App\Http\Controllers\API\CoursesController::class, 'destroy'])->name('delete-course');


Route::get('/reviews/{id}', [App\Http\Controllers\API\ProgramReviewsController::class, 'index'])->name('reviews');
Route::post('/reviews', [App\Http\Controllers\API\ProgramReviewsController::class, 'store'])->name('reviews-store');



Route::get('add-resources', [App\Http\Controllers\API\ResourceController::class, 'index'])->name('resources');
Route::post('resources', [App\Http\Controllers\API\ResourceController::class, 'store'])->name('resources-store');



});


});
